import React, { Component } from 'react';
import axios from 'axios';
import PlayArrow from "@material-ui/icons/PlayArrow";
import Fab from '@material-ui/core/Fab';
import ProgressBar from "../progress/ProgressBar";
import SimpleSelect from "../select/SimpleSelect";
import { openNotification, baseURL } from '../../utils';
import store from '../../redux/store';
import { exploitsActions, selectActions } from "../../redux/actions";







export default class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            taskIP: '',
            taskIPfrom: '',
            taskIPto: '',
            taskPortsTCP: '',
            taskPortsUDP: '',
            taskScripts: null,
            taskParams: 'qwe',
            isOneIp: true,
            isRangeIp: false,
            isIpFromFile: false,
            isMethodOne: false,
            isMethodTwo: false,
            isMethodThree: false,
            isLoading: false,
            isTCPneed: false,
            isUDPneed: false,
            file: ''
        };
        this.handleChangeIp = this.handleChangeIp.bind(this);
        this.handleChangePortsTCP = this.handleChangePortsTCP.bind(this);
        this.handleChangePortsUDP = this.handleChangePortsUDP.bind(this);
        this.PortsTCPisNeeded = this.PortsTCPisNeeded.bind(this);
        this.PortsUDPisNeeded = this.PortsUDPisNeeded.bind(this);
        this.handleChangeParams = this.handleChangeParams.bind(this);
        this.handleRadioOneClick = this.handleRadioOneClick.bind(this);
        this.handleRadioTwoClick = this.handleRadioTwoClick.bind(this);
        this.handleRadioThreeClick = this.handleRadioThreeClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleMethodOne = this.handleMethodOne.bind(this);
        this.handleMethodTwo = this.handleMethodTwo.bind(this);
        this.handleMethodThree = this.handleMethodThree.bind(this);
        this.selectTypeIp = this.selectTypeIp.bind(this);
        this.handleGetFile = this.handleGetFile.bind(this);
    }

    componentDidMount() {
        if(this.props.scanName.length < 1){
            openNotification({
                title: 'Ошибка!',
                text: 'Вернитесь на стартовую страницу сканера.',
                type: 'error',
            });
        }
        else if (this.props.isScannerOne.length < 1 || this.props.isScannerTwo.length < 1){
            openNotification({
                title: 'Ошибка!',
                text: 'Вернитесь на стартовую страницу сканера.',
                type: 'error',
            });
        }

    }

    handleGetFile = e => {
        e.preventDefault();
        this.setState({isLoading:true});

        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            this.setState({
                file: reader.result,
                isLoading: false
            });
        };

        reader.readAsDataURL(file);
        openNotification({
            title: 'Успех!',
            text: 'Файл успешно добавлен.',
            type: 'success',
        });
    };

    PortsTCPisNeeded() {
        this.setState({
            isTCPneed: !this.state.isTCPneed,
        })
    }

    PortsUDPisNeeded() {
        this.setState({
            isUDPneed: !this.state.isUDPneed,
        })
    }

    handleMethodOne() {
        this.setState({
            isMethodOne: !this.state.isMethodOne,
        })
    }

    handleMethodTwo() {
        this.setState({
            isMethodTwo: !this.state.isMethodTwo,
        })
    }

    handleMethodThree() {
        this.setState({
            isMethodThree: !this.state.isMethodThree,
        })
    }


    handleRadioOneClick() {
        this.setState({
            isOneIp: true,
            isRangeIp: false,
            isIpFromFile: false
        })
    }

    handleRadioTwoClick() {
        this.setState({
            isOneIp: false,
            isRangeIp: true,
            isIpFromFile: false
        })
    }

    handleRadioThreeClick() {
        this.setState({
            isOneIp: false,
            isRangeIp: false,
            isIpFromFile: true
        })
    }


    handleChangeIp(taskIP) {

        this.setState({taskIP});
    }
    handleChangeIpFrom(taskIPfrom) {

        this.setState({taskIPfrom});
    }
    handleChangeIpTo(taskIPto) {

        this.setState({taskIPto});
    }

    handleChangePortsTCP(taskPortsTCP) {
        this.setState({taskPortsTCP});
    }

    handleChangePortsUDP(taskPortsUDP) {
        this.setState({taskPortsUDP});
    }

    handleChangeParams(taskParams) {
        this.setState({taskParams});
    }

    globalDispatch(selectedNode) {
        store.dispatch(selectActions.setSelectedNode(selectedNode));
    }



    handleSubmit() {
        this.setState({
            isLoading: true
        });

        if(this.state.isOneIp) {
            if(this.state.taskIP.length < 1) {
                openNotification({
                    title: 'Ошибка!',
                    text: 'IP адрес не может быть пустым.',
                    type: 'error',
                });
                this.setState({
                    isLoading: false
                });
            }
        }
        else if(this.state.isRangeIp) {
            if(this.state.taskIPfrom.length < 1 || this.state.taskIPto.length < 1) {
                openNotification({
                    title: 'Ошибка!',
                    text: 'IP адрес не может быть пустым.',
                    type: 'error',
                });
                this.setState({
                    isLoading: false,
                    taskIP : "1"
                });
            }
        }

        else if (this.state.isIpFromFile) {
            if(this.state.file.length < 1) {
                openNotification({
                    title: 'Ошибка!',
                    text: 'Выберите файл',
                    type: 'error',
                });
                this.setState({
                    isLoading: false,
                    taskIP : "1"
                });
            }
        }

            var scannerOne;
            var scannerTwo;
            if(this.props.isScannerOne)
                scannerOne = 'True';
            else scannerOne = 'False';
            if(this.props.isScannerTwo)
                scannerTwo = 'True';
            else scannerTwo = 'False';

            axios.post(baseURL+`/api/v0/scanner/`,
                {
                    'action': 'scan',
                    'scanName': this.props.scanName,
                    'taskName': this.props.scanName,
                    'taskNMAP': scannerOne,
                    'taskOpenVAS': scannerTwo,
                    'taskIP': this.state.taskIP,
                    'taskPORTS': this.state.taskPORTS,
                    "taskScripts": 'qwe',
                    'taskParams': this.state.taskParams,
                    'file' : this.state.file,
                    'taskFile' : this.state.isIpFromFile
                }
            ).then(res => {
                this.setState({
                    isLoading: false
                });
                this.props.scrolling.scrollToNext();
                let nodes = res.data.Node;
                let links = res.data.Link;
                window.init(nodes,links);
                store.dispatch(exploitsActions.setExploitsData(res.data.Vulns));
                window.globalScroller =  this.props.scrolling.scrollToNext;
                window.globalDispatch = this.globalDispatch;

            })
                .catch( error => {
                    this.setState({
                        isLoading: false
                    });
                    openNotification({
                        title: 'Ошибка!',
                        text: `${error}`,
                        type: 'error',
                    });
                })

    }

    selectTypeIp(value) {

        if (value === 'IP') {
            this.setState({
                isOneIp: true,
                isRangeIp: false,
                isIpFromFile: false
            });
        } else if (value === 'Диапазон IP') {
            this.setState({
                isOneIp: false,
                isRangeIp: true,
                isIpFromFile: false
            });


        } else if (value === 'Из файла') {
            this.setState({
                isOneIp: false,
                isRangeIp: false,
                isIpFromFile: "True"
            });
        }
    }





    render() {


        let ipContainer = null;
        if (this.state.isOneIp) {
            ipContainer = <div className="ip target">
                <p><span>IP</span> адрес цели:</p>

                <input id="target" type="text"
                       onChange={event => this.handleChangeIp(event.target.value)}
                />
            </div>
        } else if (this.state.isRangeIp) {
            ipContainer = <div>
                <div className="ip">
                    <p><span>От</span>:</p>
                    <input id="targetFrom" type="text"
                           onChange={event => this.handleChangeIpFrom(event.target.value)}
                    />
                </div>
                <div className="ip">
                    <p><span>До</span>:</p>
                    <input id="targetTo" type="text"
                           onChange={event => this.handleChangeIpTo(event.target.value)}
                    />
                </div>
            </div>
        } else if (this.state.isIpFromFile)
            ipContainer = <div id="upload-container" onClick={() => debouncedUpload()}>
                <div className="img-container">
                    <svg viewBox="64 64 896 896" focusable="false" className="" data-icon="plus" width="1em" height="1em"
                                                    fill="currentColor" aria-hidden="true">
                        <path d="M482 152h60q8 0 8 8v704q0 8-8 8h-60q-8 0-8-8V160q0-8 8-8z"></path>
                        <path d="M176 474h672q8 0 8 8v60q0 8-8 8H176q-8 0-8-8v-60q0-8 8-8z"></path>
                    </svg>
                </div>

                    <div>
                        <input id="file-input" type="file" name="file" multiple onChange={e => this.handleGetFile(e)}/>
                            <label id ="choose-label" htmlFor="file-input"/>
                            <p>Выберите файл</p>
                    </div>
            </div>;

        let isLoading = null;
        if (this.state.isLoading)
            isLoading = <ProgressBar/>;
        else isLoading = <Fab size="small" color="primary" aria-label="Add">
            <PlayArrow onClick={this.handleSubmit}/>
        </Fab>;

        let PORTSTCP = null;
        if (this.state.isTCPneed)
            PORTSTCP = <div className="ip ports">
                <p>Порты <span>TCP</span>:</p>
                <input id="ports" type="text"
                       onChange={event => this.handleChangePortsTCP(event.target.value)}
                />
            </div>;
        let PORTSUDP = null;
        if (this.state.isUDPneed)
            PORTSUDP = <div className="ip ports">
                <p>Порты <span>UDP</span>:</p>
                <input id="ports" type="text"
                       onChange={event => this.handleChangePortsUDP(event.target.value)}
                />
            </div>;

        const content = ['IP', 'Диапазон IP', 'Из файла'];
        return (
            <React.Fragment>
                <header id="home">
                    <div className="start-scan">
                        <div className="enter-data">
                            <div className="source">
                                <div className="enter-left">
                                    <div className="title-select">
                                        <p>Входные данные</p>
                                        <SimpleSelect content={content} name={"Выбор IP"}
                                                      select={this.selectTypeIp}/></div>
                                    {ipContainer}
                                    {PORTSTCP}
                                    {PORTSUDP}
                                </div>

                            </div>

                            <div className="play">
                                {isLoading}
                            </div>
                        </div>


                        <div className="methods">
                            <div id="1" className="method">
                                <label>
                                    <input type="checkbox" id="r1" name="radio"/>
                                    <span className="checkbox-custom" onClick={this.handleMethodOne}/>
                                </label>
                                <p>По умолчанию</p>
                            </div>
                            <div id="2" className="method">
                                <label>
                                    <input type="checkbox" id="r2" name="radio"/>
                                    <span className="checkbox-custom" onClick={this.handleMethodTwo}/>
                                </label>
                                <p>По умолчанию</p>
                            </div>
                            <div id="3" className="method">
                                <label>
                                    <input type="checkbox" id="r3" name="radio"/>
                                    <span className="checkbox-custom" onClick={this.handleMethodThree}/>
                                </label>
                                <p>По умолчанию</p>
                            </div>
                            <div id="TCP" className="method">
                                <label>
                                    <input type="checkbox" id="tcp" name="radio"/>
                                    <span className="checkbox-custom" onClick={this.PortsTCPisNeeded}/>
                                </label>
                                <p>TCP порты</p>
                            </div>
                            <div id="UDP" className="method">
                                <label>
                                    <input type="checkbox" id="udp" name="radio"/>
                                    <span className="checkbox-custom" onClick={this.PortsUDPisNeeded}/>
                                </label>
                                <p>UDP порты</p>
                            </div>
                        </div>

                        </div>


                    <nav
                        id="nav-wrap">
                        <a
                            className="mobile-btn"
                            href="#nav-wrap"
                            title="Show navigation"> Показать </a>
                        <a className="mobile-btn" href="#" title="Hide navigation">Скрыть</a>
                        <ul
                            id="nav"
                            className="nav">
                            <li
                                className="current"><a
                                className="smoothscroll"
                                href="#">Сканирование: {this.props.scanName}</a></li>
                        </ul>
                    </nav>

                    <div
                        className="row banner">
                    </div>

                </header>
            </React.Fragment>
        );
    }
}





function handleUploadClick () {
    let label = document.getElementById("choose-label");
    label.click();
}


function debounce(f, ms) {

    let isCooldown = false;

    return function() {
        if (isCooldown) return;

        f.apply(this, arguments);

        isCooldown = true;

        setTimeout(() => isCooldown = false, ms);
    };

}



let debouncedUpload = debounce(handleUploadClick, 1000);