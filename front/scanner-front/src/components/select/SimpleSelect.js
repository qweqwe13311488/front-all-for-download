import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';



const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        fontSize: '20px'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 150,
        fontSize: '20px'
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 50,
        fontSize: '20px'
    },
    select: {
        marginTop: '0',
        fontSize: '15px'
    },
    selectMenu: {
        fontSize: '17px'
    }
});

class SimpleSelect extends React.Component {
    state = {
        payload: '',
        name: 'Meter',
        labelWidth: 0,
    };

    componentDidMount() {
        this.setState({
        });
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
        this.props.select(event.target.value);
    };

    render() {
        const { classes } = this.props;
        const  content  = this.props.content;
        const listItems = content.map((value,index) =>
             <MenuItem key={index} value={value}>{value}</MenuItem>
         );
        return (
            <form className={classes.root} autoComplete="off">
                <FormControl className={classes.formControl}>
                    <InputLabel className={classes.select} htmlFor="payload-simple">{this.props.name}</InputLabel>
                    <Select
                        value={this.state.payload}
                        onChange={this.handleChange}
                        inputProps={{
                            name: 'payload',
                            id: 'payload-simple',
                        }}
                        className={classes.selectMenu}
                    >
                        <MenuItem value="">
                            <em>Пусто</em>
                        </MenuItem>
                        {listItems}
                    </Select>
                </FormControl>
            </form>
        );
    }
}



export default withStyles(styles)(SimpleSelect);