import React from 'react';



export default class extends React.Component {

    render() {

        return (

            <div id="myDiagramDiv"
                 style={{
                     flexGrow: '1',
                     height: '90%',
                     hWidth: '90%',
                     border: 'solid 1px black',
                     display: 'flex',
                     justifyContent: 'center'

                 }}/>

        );
    }
}