const Actions = {
    setSelectedNode: data => ({
        type: 'NODE:SET_SELECTED',
        payload: data,
    }),
};

export default Actions;
