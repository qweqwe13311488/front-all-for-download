import React, {Component} from 'react';
import Header from '../../components/header/header';
import ExploitConfig from "../../components/exploit/ExploitConfig";
import { Scroller, scrollInitalState } from 'react-skroll'
import NetworkGraph from "../networkGraph/networkGraph";
import MyTerminal from "../terminal/terminal";




class AppContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            scroll: scrollInitalState,

        };

    }

    render() {
        const {scroll} = this.state;
        return (
            <div className="App">
                <div className="wrapper">
                    <Scroller
                        scrollRef={ref => this.scroll = ref}
                        autoScroll={true}
                        autoFrame={true}
                        onScrollChange={(scroll) => this.setState({scroll})}
                    >
                        <section key="1" style={{height: '100%'}}>

                            <Header scrolling={this.scroll} scanName={this.props.scanName} isScannerOne={this.props.isScannerOne} isScannerTwo={this.props.isScannerTwo}/>

                        </section>
                        <section key="2" style={{height: '100%'}}>

                            <NetworkGraph/>

                        </section>
                        <section key="3" style={{height: '100%'}}>

                            <ExploitConfig exploits={this.props.exploits}/>

                            <div className="terminalCase">
                                {/*<MyTerminal/>*/}
                            </div>

                        </section>

                    </Scroller>
                </div>

            </div>
        );
    }
}

export default AppContainer;
