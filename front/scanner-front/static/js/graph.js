


// storage for names of exploitation nodes
let targetNode = '';


// init goJS sample for network graph
function init(nodes,links) {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    diagram =
        $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
                // initialContentAlignment: go.Spot.Center,
                // layout: $(go.CircularLayout),
                // // moving and copying nodes also moves and copies their subtrees
                // "commandHandler.copiesTree": true,  // for the copy command
                // "commandHandler.deletesTree": true, // for the delete command
                // "draggingTool.dragsTree": true,  // dragging for both move and copy
                // "undoManager.isEnabled": true
            });


    function changeCategory(obj, category) {
        let node = obj.part;
        if (node) {
            let diagram = node.diagram;
            diagram.initialDocumentSpot = go.Spot.Center;
            diagram.initialViewportSpot = go.Spot.Center;
            diagram.startTransaction("changeCategory");
            diagram.model.setCategoryForNodeData(node.data, category);
            diagram.commitTransaction("changeCategory");
        }
    }

    function findHeadShot(type) {
        return "../static/img/" + type + ".png"
    }

    function exploitInit(e, obj) {
        let contextMenu = obj.part;
        targetNode = contextMenu.data.key;
        window.globalDispatch(targetNode);
        window.globalScroller();
    }

    var hope =
        $(go.Node, "Spot",
            $(go.Panel, "Auto",
                $(go.Shape, "Ellipse",
                    new go.Binding("fill", "color")),
                $(go.Panel, "Vertical",
                    {margin: 4},
                    $(go.TextBlock, {row: 0, column: 0, columnSpan: 2, font: "bold 11pt sans-serif"},
                        new go.Binding("text", "key")))
            ),
        );


    var name =
        $(go.Node, "Spot",
            {
                doubleClick: exploitInit
            },
            $(go.Panel, "Auto",
                $(go.Shape, "Rectangle",
                    new go.Binding("fill", "color")),
                $(go.Panel, "Vertical",
                    {margin: 4},
                    $(go.Picture,
                        {
                            name: "Picture",
                            desiredSize: new go.Size(50, 50),
                        },
                        new go.Binding("source", "deviceType", findHeadShot)),
                    $(go.Panel, "Horizontal",
                        {margin: 3},
                        $("Button",
                            {margin: 2},
                            $(go.TextBlock, "Basic info", {font: "bold 11pt sans-serif"}), {
                                click: function (e, obj) {
                                    changeCategory(obj, 'basics');
                                }
                            }),
                        $("Button",
                            {margin: 2},
                            $(go.TextBlock, "Port info", {font: "bold 11pt sans-serif"}), {
                                click: function (e, obj) {
                                    changeCategory(obj, 'ports');
                                }
                            })
                    ),
                    $(go.TextBlock, {row: 0, column: 0, columnSpan: 2, font: "bold 11pt sans-serif"},
                        new go.Binding("text", "key")))
            ),
            $("TreeExpanderButton",
                {alignment: go.Spot.Bottom, alignmentFocus: go.Spot.Top},
                {visible: true}),

        );

    var basics =
        $(go.Node, "Auto",
            {
                doubleClick: exploitInit
            },
            $(go.Shape, "Rectangle",
                new go.Binding("fill", "color")),
            $(go.Panel, "Vertical",
                {margin: 4},
                $(go.Picture,
                    {
                        name: "Picture",
                        desiredSize: new go.Size(50, 50),
                        // margin: new go.Margin(6, 8, 6, 10),
                    },
                    new go.Binding("source", "deviceType", findHeadShot)),
                $(go.Panel, "Horizontal",
                    {margin: 3},
                    $("Button",
                        {margin: 2},
                        $(go.TextBlock, "Basic info", {font: "bold 11pt sans-serif"}), {
                            click: function (e, obj) {
                                changeCategory(obj, 'basics');
                            }
                        }),
                    $("Button",
                        {margin: 2},
                        $(go.TextBlock, "Port info", {font: "bold 11pt sans-serif"}), {
                            click: function (e, obj) {
                                changeCategory(obj, 'ports');
                            }
                        })
                ),
                $(go.Panel, "Table",
                    {defaultAlignment: go.Spot.Left},
                    $(go.TextBlock, {row: 0, column: 0, columnSpan: 2, font: "bold 11pt sans-serif"},
                        new go.Binding("text", "key")),
                    $(go.TextBlock, {row: 1, column: 1, columnSpan: 2, font: "10pt sans-serif"}, new go.Binding("text", "basics"))
                ),
                $("TreeExpanderButton",
                    {alignment: go.Spot.Bottom, alignmentFocus: go.Spot.Top},
                    {visible: true})
            )
        );
    var ports =
        $(go.Node, "Auto",
            {
                doubleClick: exploitInit
            },
            $(go.Shape, "Rectangle",
                new go.Binding("fill", "color")),
            $(go.Panel, "Vertical",
                {margin: 4},
                $(go.Picture,
                    {
                        name: "Picture",
                        desiredSize: new go.Size(50, 50),
                        // margin: new go.Margin(6, 8, 6, 10),
                    },
                    new go.Binding("source", "deviceType", findHeadShot)),
                $(go.Panel, "Horizontal",
                    {margin: 3},
                    $("Button",
                        {margin: 2},
                        $(go.TextBlock, "Basic info", {font: "bold 11pt sans-serif"}), {
                            click: function (e, obj) {
                                changeCategory(obj, 'basics');
                            }
                        }),
                    $("Button",
                        {margin: 2},
                        $(go.TextBlock, "Port info", {font: "bold 11pt sans-serif"}), {
                            click: function (e, obj) {
                                changeCategory(obj, 'ports');
                            }
                        })
                ),
                $(go.Panel, "Table",
                    {defaultAlignment: go.Spot.Left},
                    $(go.TextBlock, {row: 0, column: 0, columnSpan: 2, font: "bold 11pt sans-serif"},
                        new go.Binding("text", "key")),
                    $(go.TextBlock, {
                        row: 1,
                        column: 1,
                        font: "10pt sans-serif"
                    }, new go.Binding("text", "ports")),
                    $(go.TextBlock, {
                        row: 2,
                        column: 1,
                        font: "10pt sans-serif"
                    }, new go.Binding("text", "vuln")),
                ),
                $("TreeExpanderButton",
                    {alignment: go.Spot.Bottom, alignmentFocus: go.Spot.Top},
                    {visible: true})
            )
        );


    diagram.initialDocumentSpot = go.Spot.Top;
    diagram.initialViewportSpot = go.Spot.Top;
    var templmap = new go.Map("string", go.Node);
    templmap.add("name", name);
    templmap.add("basics", basics);
    templmap.add("ports", ports);
    templmap.add("hope", hope);

    diagram.nodeTemplateMap = templmap;

    diagram.layout =
        $(go.TreeLayout,
            {

                treeStyle: go.TreeLayout.StyleLastParents,
                arrangement: go.TreeLayout.ArrangementHorizontal,
                // properties for most of the tree:
                angle: 90,
                layerSpacing: 35,
                // properties for the "last parents":
                alternateAngle: 90,
                alternateLayerSpacing: 35,
                alternateAlignment: go.TreeLayout.AlignmentBus,
                alternateNodeSpacing: 20
            });
    diagram.linkTemplate =
        $(go.Link,
            $(go.Shape, {stroke: "black"}));
    diagram.initialScale = 0.7;
    diagram.model.nodeDataArray = nodes;
    diagram.model.linkDataArray = links;
}

