function init() {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;

    var blues = ['#E1F5FE', '#B3E5FC', '#81D4FA', '#4FC3F7', '#29B6F6', '#03A9F4', '#039BE5', '#0288D1', '#0277BD', '#01579B'];

    myDiagram =
        $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
                initialAutoScale: go.Diagram.UniformToFill,
                contentAlignment: go.Spot.Center,
                layout: $(go.ForceDirectedLayout),
                // moving and copying nodes also moves and copies their subtrees
                "commandHandler.copiesTree": true,  // for the copy command
                "commandHandler.deletesTree": true, // for the delete command
                "draggingTool.dragsTree": true,  // dragging for both move and copy
                "undoManager.isEnabled": true
            });

    // Define the Node template.
    // This uses a Spot Panel to position a button relative
    // to the ellipse surrounding the text.
    myDiagram.nodeTemplate =
        $(go.Node, "Spot",
            {
                selectionObjectName: "PANEL",
                isTreeExpanded: false,
                isTreeLeaf: false
            },
            // the node's outer shape, which will surround the text
            $(go.Panel, "Auto",
                { name: "PANEL" },
                $(go.Shape, "Circle",
                    { fill: "whitesmoke", stroke: "black" },
                    new go.Binding("fill", "rootdistance", function(dist) {
                        dist = Math.min(blues.length - 1, dist);
                        return blues[dist];
                    })),
                $(go.TextBlock,
                    { font: "12pt sans-serif", margin: 5 },
                    new go.Binding("text", "key"))
            ),
            // the expand/collapse button, at the top-right corner
            $("TreeExpanderButton",
                {
                    name: 'TREEBUTTON',
                    width: 20, height: 20,
                    alignment: go.Spot.TopRight,
                    alignmentFocus: go.Spot.Center,
                    // customize the expander behavior to
                    // create children if the node has never been expanded
                    click: function (e, obj) {  // OBJ is the Button
                        var node = obj.part;  // get the Node containing this Button
                        if (node === null) return;
                        e.handled = true;
                        expandNode(node);
                    }
                }
            )  // end TreeExpanderButton
        );  // end Node

    // create the model with a root node data
    myDiagram.model = new go.TreeModel([
        { key: 0, color: blues[0], everExpanded: false }
    ]);

    // setup the tree view; will be initialized with data by the load() function
    myTreeView =
        $(go.Diagram, "myTreeView",
            {
                allowMove: false,  // don't let users mess up the tree
                allowCopy: true,  // but you might want this to be false
                "commandHandler.copiesTree": true,
                "commandHandler.copiesParentKey": true,
                allowDelete: true,  // but you might want this to be false
                "commandHandler.deletesTree": true,
                allowHorizontalScroll: false,
                layout:
                    $(go.TreeLayout,
                        {
                            alignment: go.TreeLayout.AlignmentStart,
                            angle: 0,
                            compaction: go.TreeLayout.CompactionNone,
                            layerSpacing: 16,
                            layerSpacingParentOverlap: 1,
                            nodeIndent: 2,
                            nodeIndentPastParent: 0.88,
                            nodeSpacing: 0,
                            setsPortSpot: false,
                            setsChildPortSpot: false,
                            arrangementSpacing: new go.Size(0, 0)
                        }),
                // when a node is selected in the tree, select the corresponding node in the main diagram
                "ChangedSelection": function(e) {
                    if (myChangingSelection) return;
                    myChangingSelection = true;
                    var diagnodes = new go.Set();
                    myTreeView.selection.each(function(n) {
                        diagnodes.add(myDiagram.findNodeForData(n.data));
                    });
                    myDiagram.clearSelection();
                    myDiagram.selectCollection(diagnodes);
                    myChangingSelection = false;
                }
            });

    myTreeView.nodeTemplate =
        $(go.Node,
            // no Adornment: instead change panel background color by binding to Node.isSelected
            { selectionAdorned: false },
            $("TreeExpanderButton",
                {
                    width: 14,
                    "ButtonBorder.fill": "white",
                    "ButtonBorder.stroke": null,
                    "_buttonFillOver": "rgba(0,128,255,0.25)",
                    "_buttonStrokeOver": null
                }),
            $(go.Panel, "Horizontal",
                { position: new go.Point(16, 0) },
                new go.Binding("background", "isSelected", function(s) { return (s ? "lightblue" : "white"); }).ofObject(),
                // Icon is not needed?
                //$(go.Picture,
                //  {
                //    width: 14, height: 14,
                //    margin: new go.Margin(0, 4, 0, 0),
                //    imageStretch: go.GraphObject.Uniform,
                //    source: "images/50x40.png"
                //  }),
                $(go.TextBlock,
                    { editable: true },
                    new go.Binding("text").makeTwoWay())
            )  // end Horizontal Panel
        );  // end Node

    // without lines
    myTreeView.linkTemplate = $(go.Link);

    // cannot share the model itself, but can share all of the node data from the main Diagram,
    // pretending the "group" relationship is the "tree parent" relationship
    myTreeView.model = $(go.TreeModel, { nodeParentKeyProperty: "group" });

    myTreeView.addModelChangedListener(function(e) {
        if (e.model.skipsUndoManager) return;
        if (myChangingModel) return;
        myChangingModel = true;
        // don't need to start/commit a transaction because the UndoManager is shared with myDiagram
        if (e.modelChange === "nodeGroupKey" || e.modelChange === "nodeParentKey") {
            // handle structural change: tree parent/children
            var node = myDiagram.findNodeForData(e.object);
            if (node !== null) node.updateRelationshipsFromData();
        } else if (e.change === go.ChangedEvent.Property) {
            // propagate simple data property changes back to the main Diagram
            var node = myDiagram.findNodeForData(e.object);
            if (node !== null) node.updateTargetBindings();
        } else if (e.change === go.ChangedEvent.Insert && e.propertyName === "nodeDataArray") {
            // pretend the new data isn't already in the nodeDataArray for the main Diagram model
            myDiagram.model.nodeDataArray.splice(e.newParam, 1);
            // now add to the myDiagram model using the normal mechanisms
            myDiagram.model.addNodeData(e.newValue);
        } else if (e.change === go.ChangedEvent.Remove && e.propertyName === "nodeDataArray") {
            // remove the corresponding node from the main Diagram
            var node = myDiagram.findNodeForData(e.oldValue);
            if (node !== null) myDiagram.remove(node);
        }
        myChangingModel = false;
    });

    load();
}