const path = require("path");
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.join(__dirname, "/dist"),
        publicPath: '/',
        filename: "index_bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    devServer: {
        historyApiFallback: true,
    },
    plugins: [
        new CopyPlugin([
            { from: 'static/css', to: 'static/css' },
            { from: 'static/js', to: 'static/js' },
            { from: 'static/img', to: 'static/img' }
        ]),
        new HtmlWebpackPlugin({
            template: "./static/index.html"
        })
    ],

};