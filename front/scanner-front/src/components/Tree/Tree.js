import React from 'react';
import {Treebeard} from 'react-treebeard';

const data = {
    name: 'root',
    toggled: true,
    children: [
        {
            name: 'parent fuck this',
            children: [
                {name: 'child1 ghg fggf'},
                {name: 'child2'}
            ]
        },
        {
            name: 'loading parent',
            loading: true,
            children: []
        },
        {
            name: 'parent',
            children: [
                {
                    name: 'nested parent',
                    children: [
                        {name: 'nested child 1'},
                        {name: 'nested child 2'}
                    ]
                }
            ]
        }
    ]
};

class Tree extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.onToggle = this.onToggle.bind(this);
    }

    onToggle(node, toggled) {
        if (this.state.cursor) {
            this.state.cursor.active = false;
        }
        node.active = true;
        if (node.children) {
            node.toggled = toggled;
        }
        this.setState({cursor: node});
    }

    render() {
        return (
            <Treebeard
                data={data}
                onToggle={this.onToggle}
                style={{tree: {
                    base: {
                    listStyle: 'none',
                    backgroundColor: 'rgb(39, 43, 77)',
                    margin: 0,
                    padding: 0,
                    color: '#fff',
                    fontFamily: 'lucida grande ,tahoma,verdana,arial,sans-serif',
                    fontSize: '14px'
                },
                    node: {
                    base: {
                    position: 'relative'
                },
                    link: {
                    cursor: 'pointer',
                    position: 'relative',
                    padding: '0px 5px',
                    display: 'block'
                },
                    activeLink: {
                    background: '#31363F'
                },
                    toggle: {
                    base: {
                    position: 'relative',
                    display: 'inline-block',
                    verticalAlign: 'top',
                    marginLeft: '-5px',
                    height: '24px',
                    width: '24px'
                },
                    wrapper: {
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    margin: '-7px 0 0 -7px',
                    height: '14px'
                },
                    height: 14,
                    width: 14,
                    arrow: {
                    fill: '#9DA5AB',
                    strokeWidth: 0
                }
                },
                    header: {
                    base: {
                    display: 'inline-block',
                    verticalAlign: 'top',
                    color: '#9DA5AB'
                },
                    connector: {
                    width: '2px',
                    height: '12px',
                    borderLeft: 'solid 2px black',
                    borderBottom: 'solid 2px black',
                    position: 'absolute',
                    top: '0px',
                    left: '-21px'
                },
                    title: {
                    lineHeight: '24px',
                    verticalAlign: 'middle'
                }
                },
                    subtree: {
                    listStyle: 'none',
                    paddingLeft: '19px'
                },
                    loading: {
                    color: '#E2C089'
                }
                }
                }}}
            />
        );
    }
}

export default Tree