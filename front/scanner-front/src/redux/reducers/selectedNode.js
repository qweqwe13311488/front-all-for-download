const initialState = {
    selectedNode: null
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case "NODE:SET_SELECTED":
            return {
                ...state,
                data: payload,
            };
        default:
            return state;
    }
};
