import { openNotification, baseURL } from '../../utils';
import axios from "axios";


const Actions = {
    setUserData: data => ({
        type: 'SCANNERS:SET_AVAILABLE',
        payload: data,
    }),
    fetchAvailableData: () => dispatch => {
        axios.post(baseURL+`/api/v0/start/`, {'get_scan_methods': 'ok'})
            .then(({ data }) => {
                dispatch(Actions.setUserData(data));
                openNotification({
                    title: 'Успех!',
                    text: 'Получены доступные сканеры.',
                    type: 'success',
                });
            })
            .catch(err => {
                openNotification({
                    title: 'Ошибка!',
                    text: 'Не удалось получить доступные сканеры.',
                    type: 'error',
                });
            });
    },
};

export default Actions;
