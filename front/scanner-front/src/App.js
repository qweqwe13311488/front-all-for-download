import React, {Component} from 'react';
import 'antd/dist/antd.css';
import ScannerStart from "./components/scannerStart/ScannerStart"
import { Route, Switch } from 'react-router-dom'
import AppContainer from "./components/app-container/AppContainer"

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            scanName: '',
            isScannerOne: '',
            isScannerTwo: ''
        };
    }

    giveScanName = (scanName,isScannerOne,isScannerTwo) => {
        this.setState({scanName, isScannerOne, isScannerTwo})
    };
    sendScanName = () => {
        return this.state.scanName
    };
    sendScannerOne = () => {
        return this.state.isScannerOne
    };
    sendScannerTwo = () => {
        return this.state.isScannerTwo
    };


    render() {
        return (
            <Switch>
                <Route exact path='/'  render={(props)=><ScannerStart {...props} giveScanName={this.giveScanName}/>} /> // route /
                <Route path='/scaner' render={(props)=><AppContainer {...props} scanName={this.sendScanName()} isScannerOne={this.sendScannerOne()} isScannerTwo={this.sendScannerTwo()} />}/>
            </Switch>
        );
    }
}

export default App;
