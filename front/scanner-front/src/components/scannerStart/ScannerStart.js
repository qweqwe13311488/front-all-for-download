import React, { Component } from 'react';
import PlayArrow from "@material-ui/icons/PlayArrow";
import Fab from '@material-ui/core/Fab';
import { Link } from 'react-router-dom';
import store from '../../redux/store';
import { connect } from 'react-redux'
import { scannersActions } from '../../redux/actions';
import { openNotification } from '../../utils';

function mapStateToProps (state) {
    return {
        scanners: state.scanners
    }
}


class ScannerStart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            scanName: '',
            isScannerOne: false,
            isScannerTwo: false
        };
        this.handleScanner = this.handleScanner.bind(this);
        this.renderMethods = this.renderMethods.bind(this);
    }


    componentDidMount() {
        store.dispatch(scannersActions.fetchAvailableData());
    }




    renderMethods (Data) {
     if(Data !== null && !!Data) {
         return Object.values(Data).map((name) =>
             <div key={name} id="UDP" className="method">
                 <label>
                     <input type="checkbox" id="udp" name="radio"/>
                     <span className="checkbox-custom" onClick={() => this.handleScanner(name)}/>
                 </label>
                 <p>{name}</p>
             </div>
         );
     }

    }

    handleChangeName = event => {
        this.setState({  scanName: event.target.value});
         this.props.giveScanName(event.target.value, this.state.isScannerOne, this.state.isScannerTwo);
    };

    handleScanner(name) {

        if(name === "Nmap"){
            this.setState({isScannerOne: !this.state.isScannerOne});
            this.props.giveScanName(this.state.scanName, !this.state.isScannerOne, this.state.isScannerTwo);
        }

        if(name === "OpenVAS"){
            this.setState({isScannerTwo: !this.state.isScannerTwo});
            this.props.giveScanName(this.state.scanName, this.state.isScannerOne, !this.state.isScannerTwo);
        }
    };


    handleClickOnFalse = () => {
        if(this.state.scanName.length < 1) {
            openNotification({
                title: 'Ошибка!',
                text: 'Название сканера не может быть пустым.',
                type: 'error',
            });
        }
        else if (this.state.isScannerOne === false && this.state.isScannerTwo === false) {
            openNotification({
                title: 'Ошибка!',
                text: 'Выберите хотябы один сканер.',
                type: 'error',
            });
        }
    };

    renderValidate = () => {
        if(this.state.scanName.length < 1) {
            return <Fab size="small" color="primary" aria-label="Add" onClick={() => this.handleClickOnFalse()}>
                        <PlayArrow/>
                    </Fab>

        }
        else if (this.state.isScannerOne === false && this.state.isScannerTwo === false) {

            return <Fab size="small" color="primary" aria-label="Add" onClick={() => this.handleClickOnFalse()}>
                <PlayArrow/>
            </Fab>
        }
        else
        {
            return <Link to={`/scaner/${this.state.scanName}`}>
                <Fab size="small" color="primary" aria-label="Add">
                    <PlayArrow/>
                </Fab>
            </Link>
        }

    };


    render() {

        return (
            <React.Fragment>
                <div className="start-wrapper">
                    <div id="scanner-start">
                        <p className="scanner-title">Super Scanner v1.0</p>
                        <div className="left-right-container">
                            <div className="start-left">
                                <div className="ip">
                                    <p>Название скана:</p>
                                    <input id="target" type="text" value={this.state.scanName} onChange={this.handleChangeName}/>
                                </div>
                            </div>
                            <div className="start-right">
                                <p>Доступные сканеры:</p>
                                {this.renderMethods(this.props.scanners.data)}
                            </div>
                        </div>

                        <div className="exploit-play">
                            <div className="exploit-run">
                                {this.renderValidate()}
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps)(ScannerStart)