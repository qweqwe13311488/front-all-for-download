import { combineReducers } from "redux";

const reducers = ["scanners","exploits","selectedNode"];

export default combineReducers(
    reducers.reduce((initial, name) => {
        initial[name] = require(`./${name}`).default;
        return initial;
    }, {})
);
