function init() {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    var blues = ['#E1F5FE', '#B3E5FC', '#81D4FA', '#4FC3F7', '#29B6F6', '#03A9F4', '#039BE5', '#0288D1', '#0277BD', '#01579B'];

    myDiagram =
        $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
                initialAutoScale: go.Diagram.UniformToFill,
                contentAlignment: go.Spot.Center,
                layout: $(go.ForceDirectedLayout),
                // moving and copying nodes also moves and copies their subtrees
                "commandHandler.copiesTree": true,  // for the copy command
                "commandHandler.deletesTree": true, // for the delete command
                "draggingTool.dragsTree": true,  // dragging for both move and copy
                "undoManager.isEnabled": true
            });

    // Define the Node template.
    // This uses a Spot Panel to position a button relative
    // to the ellipse surrounding the text.
    myDiagram.nodeTemplate =
        $(go.Node, "Spot",
            {
                selectionObjectName: "PANEL",
                isTreeExpanded: false,
                isTreeLeaf: false
            },
            // the node's outer shape, which will surround the text
            $(go.Panel, "Auto",
                { name: "PANEL" },
                $(go.Shape, "Circle",
                    { fill: "whitesmoke", stroke: "black" },
                    new go.Binding("fill", "rootdistance", function(dist) {
                        dist = Math.min(blues.length - 1, dist);
                        return blues[dist];
                    })),
                $(go.TextBlock,
                    { font: "12pt sans-serif", margin: 5 },
                    new go.Binding("text", "key"))
            ),
            // the expand/collapse button, at the top-right corner
            $("TreeExpanderButton",
                {
                    name: 'TREEBUTTON',
                    width: 20, height: 20,
                    alignment: go.Spot.TopRight,
                    alignmentFocus: go.Spot.Center,
                    // customize the expander behavior to
                    // create children if the node has never been expanded
                    click: function (e, obj) {  // OBJ is the Button
                        var node = obj.part;  // get the Node containing this Button
                        if (node === null) return;
                        e.handled = true;
                        expandNode(node);
                    }
                }
            )  // end TreeExpanderButton
        );  // end Node

    // create the model with a root node data
    myDiagram.model = new go.TreeModel([
        { key: 0, color: blues[0], everExpanded: false }
    ]);

}