const initialState = {
    scanners: null
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case "SCANNERS:SET_AVAILABLE":
            return {
                ...state,
                data: payload,
            };
        default:
            return state;
    }
};
